<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Animal_model extends CI_Model {

  public function getAnimal_all()
  {
    $sql = "SELECT * FROM animal";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function getAnimal_id($id)
  {
   $query = $query = $this->db->get_where('animal', array('id' => $id));
   return $query->result_array();
 }

}

/* End of file Animal_model.php */
/* Location: ./application/models/Animal_model.php */