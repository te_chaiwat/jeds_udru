<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Management_model extends CI_Model {

  public function logCheck()
  {
    $sql = "
    SELECT
    ac.id,
    ac.ac_name,
    ac.ac_lastname,
    ac.ac_school,
    am.animal_name,
    am.animal_importance,
    ck.chck_see,
    ck.create_at
    FROM  account ac
    INNER JOIN checknow ck ON ac.id = ck.user_id
    INNER JOIN animal am ON am.id = ck.animal_id
    ORDER BY ac.id
    ";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

}

/* End of file Management_model.php */
/* Location: ./application/models/Management_model.php */