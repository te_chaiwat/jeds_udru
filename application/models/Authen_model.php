<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authen_model extends CI_Model {

	public function getAccountAll($id=null)
	{
		$sql = "
		SELECT * FROM account
		";
		if($id){
			$sql .= "WHERE id = ".$id;
		}
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function createAccount($data)
	{
		$this->db->insert('account', $data);

	}

	public function updateAccount($id, $data)
	{
		$this->db->where('id',$id);
		$this->db->update('account', $data);
	}

	public function CheckValidLogin($username='',$password ='')
	{
		$sql = "
		SELECT
		id,
		ac_username,
		ac_name,
		ac_lastname,
		ac_school,'
		ac_room',
		ac_classroom,
		ac_status
		FROM account
		WHERE ac_username = '$username'
		AND ac_password = '$password'
		";

		$query = $this->db->query($sql);

		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return $query->num_rows();
		}
	}
}

/* End of file Authen_model.php */
/* Location: ./application/models/Authen_model.php */