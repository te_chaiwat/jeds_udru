<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Authen extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('authen_model');
		$this->load->database();

		$this->layout = 'frontoffice/authen/';

		if ($this->session->userdata('userID')) {
			$this->data['dataLogin'] = $this->session->userdata();
		} else {
			$this->data['dataLogin'] = null;
		}
	}

	public function index()
	{
		if(!empty($this->data['dataLogin'])){
			redirect('index.php/welcome','refresh');
		}else{
			$this->app->render('Authen JEDs UDRU', $this->layout . 'login', $this->data, false);
		}
	}

	public function checklogin()
	{
		if ($_POST) {
			$authenRS = $this->authen_model->CheckValidLogin($this->input->post('username'), trim(md5($this->input->post('password'))));
			if (!empty($authenRS)) {
				$this->setSession($authenRS);
			} else {
				$this->data['loginfail'] = $this->session->set_flashdata('message', ' Username OR PASSWORD FAIL !!');
				$this->app->render("JEDs", $this->layout . 'login', $this->data, false);
			}
		} else {
			redirect('index.php/welcome', 'refresh');
		}
	}

	public function regis()
	{
		$this->data = '';
		$this->app->render('Authen Withdraw', $this->layout . 'regis', $this->data, false);
	}

	public function addregis()
	{
		if ($_POST) {
			$data = array(
				'ac_username'  => $this->input->post('username'),
				'ac_password'  => md5($this->input->post('password')),
				'ac_name'      => $this->input->post('name'),
				'ac_lastname'  => $this->input->post('lastname'),
				'ac_school'    => $this->input->post('school'),
				'ac_class'     => $this->input->post('class'),
				'ac_classroom' => $this->input->post('classroom'),
			);

			$userRS = $this->authen_model->CheckValidLogin($data['ac_username'], trim($data['ac_password']));
			if ($userRS > 0) {
				$this->data['loginfail'] = $this->session->set_flashdata('message', ' Username นี้มีคนใช้งานแล้ว !!');
				$this->app->render("JEDs UDRU", $this->layout . 'regis', $this->data, null);
			} else {
				$add = $this->authen_model->createAccount($data);

				if (!empty($add)) {
					$authenRS = $this->authen_model->CheckValidLogin($data['ac_username'], trim($data['ac_password']));
					if (count($authenRS) > 0) {
						$this->setSession($authenRS);
					} else {
						$this->data['loginfail'] = $this->session->set_flashdata('message', ' Username OR PASSWORD FAIL !!');
						$this->app->render("JEDs UDRU", $this->layout . 'regis', $this->data, null);
					}
				} else {
					redirect('index.php/authen', 'refresh');
				}
			}
		}
	}

	public function setSession($authenRS)
	{
		$this->loginSession = array(
			'userID'   => $authenRS[0]['id'],
			"username" => $authenRS[0]['ac_username'],
			"name"     => $authenRS[0]['ac_name'],
			"lastname" => $authenRS[0]['ac_lastname'],
			'school'   => $authenRS[0]['ac_school'],
			'status'   => $authenRS[0]['ac_status'],
		);
		$this->session->set_userdata($this->loginSession);
		$this->config->set_item('sess_expiration', '0');
// เสร็จแล้วไปที่หน้า welcome
		if($this->loginSession['status'] == 'supper'){
			redirect('index.php/management','refresh');
		}elseif ($this->loginSession['status'] == 'admin') {
			redirect('index.php/management','refresh');
		} else {
			redirect('index.php/welcome', 'refresh');
		}
	}

	public function statusSupper()
	{
		$this->app->render('management', 'backoffice/management/index', $this->data, true);
	}

	public function profile($id)
	{
		$this->data['profile'] = $this->authen_model->getAccountAll($id);

		$this->app->render('Profile', $this->layout . 'profile', $this->data, false);
	}

	public function updateProfile()
	{
		$id   = $this->input->post('id');
		$data = array(
			'ac_name'      => $this->input->post('name'),
			'ac_lastname'  => $this->input->post('lastname'),
			'ac_school'    => $this->input->post('school'),
			'ac_class'     => $this->input->post('class'),
			'ac_classroom' => $this->input->post('classroom'),
		);
		$this->authen_model->updateAccount($id, $data);
		redirect('index.php/welcome', 'refresh');
	}

	public function logOut()
	{
		$userID   = $this->session->unset_userdata("userID");
		$username = $this->session->unset_userdata("username");
		$fullname = $this->session->unset_userdata("fullname");
		session_destroy();
		redirect('index.php/authen/', 'refresh');
	}

}

/* End of file Authen.php */
/* Location: ./application/controllers/Authen.php */
