<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CheckColor extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->layout = "frontoffice/checkcolor";

		if($this->session->userdata('userID')){
			$this->data['dataLogin'] = $this->session->userdata();
		}else{
			$this->data['dataLogin'] = null;
			redirect('index.php/authen','refresh');
		}
	}

	public function index()
	{
		$this->app->render('Checkcolor', 'frontoffice/checkcolor/index', $this->data , false);
	}

}

/* End of file CheckColor.php */
/* Location: ./application/controllers/CheckColor.php */