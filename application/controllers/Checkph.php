<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkph extends CI_Controller {
  public function __construct()
  {
    parent::__construct();

    $this->load->model('animal_model');
    $this->load->model('checkph_model');
    $this->layout = "frontoffice/process";

    if($this->session->userdata('userID')){
      $this->data['dataLogin'] = $this->session->userdata();
    }else{
      $this->data['dataLogin'] = null;
      redirect('index.php/authen','refresh');
    }
  }

  public function index()
  {
    $this->data['animals']= $this->animal_model->getAnimal_all();
    $this->app->render('Dashboard', 'frontoffice/dashboard/index', $this->data , false);
  }

  public function getAnimalJson()
  {
    $data = $this->animal_model->getAnimal_all();
    echo json_encode($data);
  }


  public function process()
  {

    $countAnimal = count($this->input->post('animal'));
    $countNumber = $this->input->post('number');
    $animal = array();
    $pH = '';
    $DO = '';
    $BOD = '';

    for($i=0; $i < $countAnimal; $i++){
      $data[$i] = $this->animal_model->getAnimal_id($this->input->post('animal')[$i]);
      foreach ($data[$i] as $rows) {
        if(!isset($animal[$rows['id']])){
          $animal[$rows['id']] = array(
            'animal_importance' => $rows['animal_importance'],
            'id' => $rows['id'],
            'animal_name' => $rows['animal_name'],
            'see_number' => $countNumber[$i]
          );
        }
      }
      $dataLog = array(
        'animal_id' => $this->input->post('animal')[$i],
        'chck_see' => $this->input->post('number')[$i],
        'user_id' =>  $this->data['dataLogin']['userID'],
        'create_at' => date('Y-m-d H:i:s'),
        'update_at' => date('Y-m-d H:i:s')
      );
      $this->checkph_model->logCheck($dataLog);

    }

    $maxcount =  array_keys($animal, max($animal));
    $process = $animal[$maxcount[0]];

    $see_more = $process['see_number'];

    switch ($process['animal_importance']) {
      case '100':
      if($see_more > 4 ){
        $pH =  "6.8 - 7.2";
        $DO =  "5.00 - 6.00";
        $BOD = "1.8 - 1.5";
      }elseif($see_more >= 1 && $see_more <= 4){
        $pH =  "6.3 - 7.8";
        $DO =  "> 6.00";
        $BOD = "< 1.5";
      }else{
        $pH =  "นอกช่วง 6.0 - 8.0";
        $DO =  "นอกช่วง 4.00 - 5.00";
        $BOD = "> 2.0 - 1.8";
      }
      break;
      case '99':
      if($see_more >=4 ){
        $pH =  "6.8 - 7.2";
        $DO =  "> 6.00";
        $BOD = "< 1.5";
      }elseif($see_more >= 1 && $see_more <= 4){
        $pH =  "6.0 - 8.0";
        $DO =  "4.00 - 5.00";
        $BOD = "2.0 - 1.8";
      }else{
        $pH =  "นอกช่วง 5.5 - 8.5";
        $DO =  "นอกช่วง 3.00 - 4.00";
        $BOD = "3.0 - 2.0";
      }
      break;
      case '98':
      if($see_more >=4 ){
        $pH =  "6.8 - 7.2";
        $DO =  "> 6.00";
        $BOD = "< 1.5";
      }elseif($see_more >= 1 && $see_more <= 4){
        $pH =  "6.0 - 8.0";
        $DO =  "4.00 - 5.00";
        $BOD = "2.0 - 1.8";
      }else{
        $pH =  "นอกช่วง 5.5 - 8.5";
        $DO =  "นอกช่วง 3.00 - 4.00";
        $BOD = "3.0 - 2.0";
      }
      break;
      case '97':
      if($see_more >=4 ){
        $pH =  "6.3 - 7.8";
        $DO =  "5.00 - 6.00";
        $BOD = "1.8. - 1.5";
      }elseif($see_more >= 1 && $see_more <= 4){
        $pH =  "6.0 - 8.0";
        $DO =  "4.00 - 5.00";
        $BOD = "2.0 -1.8";
      }else{
        $pH =  "นอกช่วง 5.5 - 8.5";
        $DO =  "นอกช่วง 3.00 - 4.00";
        $BOD = "3.0 - 2.0";
      }
      break;
      case '96':
      if($see_more >=4 ){
        $pH =  "6.0 - 8.0";
        $DO =  "4.00 - 5.00";
        $BOD = "2.0 - 1.8";
      }elseif($see_more >= 1 && $see_more <= 4){
        $pH =  "6.0 - 7.2";
        $DO =  "> 6.00";
        $BOD = "< 1.5";
      }else{
        $pH =  "นอกช่วง 5.5 - 8.5";
        $DO =  "นอกช่วง 3.00 - 4.00";
        $BOD = "3.0 - 2.0";
      }
      break;
      case '95':
      if($see_more >=4 ){
        $pH =  "6.0 - 8.0";
        $DO =  "4.00 - 5.00";
        $BOD = "2.0 - 1.8";
      }elseif($see_more >= 1 && $see_more <= 4){
        $pH =  "6.8 - 7.2";
        $DO =  "> 6.00";
        $BOD = "< 1.5";
      }else{
        $pH =  "นอกช่วง 5.5 - 8.5";
        $DO =  "นอกช่วง 3.00 - 4.00";
        $BOD = "2.0";
      }
      break;
      case '94':
      if($see_more >=4 ){
        $pH =  "5.5 - 8.5";
        $DO =  "3.00 - 4.00";
        $BOD = "3.0 - 2.0";
      }elseif($see_more >= 1 && $see_more <= 4){
        $pH =  "6.0 - 8.0";
        $DO =  "4.00 - 5.00";
        $BOD = "2.0 - 1.8";
      }else{
        $pH =  "นอกช่วง 5.0 - 9.0";
        $DO =  "นอกช่วง 2.00 - 3.00";
        $BOD = "> 4.00";
      }
      break;
      case '93':
      if($see_more >=4 ){
        $pH =  "5.5 - 8.5";
        $DO =  "3.00 - 4.00";
        $BOD = "3.0 - 2.0";
      }elseif($see_more >= 1 && $see_more <= 4){
        $pH =  "6.0 - 8.0";
        $DO =  "4.00 - 5.00";
        $BOD = "2.0 - 1.8";
      }else{
        $pH =  "นอกช่วง 5.0 - 9.0";
        $DO =  "นอกช่วง 2.00 - 3.00";
        $BOD = "> 4.00";
      }
      break;
      default:
      $pH =  "นอกช่วง 5.0 - 9.0";
      $DO =  "นอกช่วง 2.00 - 3.00";
      $BOD = "> 4.00";
      break;
    }
    $this->data['pH'] = $pH;
    $this->data['DO'] = $DO;
    $this->data['BOD'] = $BOD;
    $this->app->render('Dashboard', $this->layout.'/index', $this->data , false);
  }

}

/* End of file Checkph.php */
/* Location: ./application/controllers/Checkph.php */