<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Management extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('authen_model');
		$this->load->model('management_model');
		$this->layout = '/backoffice/management/';

		if(empty($this->session->userdata()) || $this->session->userdata('status') == 'public' ){
			$this->data['dataLogin'] = null;
			redirect('index.php/authen','refresh');
		}else{
			$this->data['dataLogin'] = $this->session->userdata();
		}
	}
	public function index()
	{
		$this->app->render('จัดการข้อมูล', $this->layout.'index', $this->data, TRUE);
	}

	public function loglist()
	{
		$this->app->render('จัดการข้อมูล', $this->layout.'index', $this->data, TRUE);
	}

	public function manageUser()
	{
		$this->data['userAll'] = $this->authen_model->getAccountAll();
		$this->app->render('จัดการข้อมูล', $this->layout.'user', $this->data, TRUE);
	}

	public function history()
	{
		$list_array = array();
		// $this->data['list'] = $this->management_model->logCheck();
		foreach($this->management_model->logCheck()  as $key => $row){
			if(isset($list_array[$row['id']])){
				array_push($list_array[$row['id']]['animals'], array(
					'animal_name' => $row['animal_name'],
					'animal_importance' => $row['animal_importance'],
					'chck_see' => $row['chck_see'],
					// 'importance' => $this->process(max(array($row['animal_importance'])),$row['chck_see'])
				));
				continue;
			}
			if(!isset($list_array[$row['id']])){
				$list_array[$row['id']] = array(
					'id' => $row['id'],
					'ac_name' => $row['ac_name'],
					'ac_lastname' => $row['ac_lastname'],
					'ac_school' => $row['ac_school'],
					'create_at' => $row['create_at'],
					'animals' => array(
						$key => array(
							'animal_name' => $row['animal_name'],
							'animal_importance' => $row['animal_importance'],
							'chck_see' => $row['chck_see'],
							// 'importance' => $this->process(max(array($row['animal_importance'])),$row['chck_see'])
						)
					)
				);
			}
		}


		$this->data['list'] = $list_array;
		$this->app->render('จัดการข้อมูล', $this->layout.'history', $this->data, TRUE);
	}

	public function process($value='', $see_more='')
	{
		// echo "<pre>";
		// return ($value);
		// exit;


		$pH ='';
		$DO ='';
		$BOD ='';

		switch ($value) {
			case '100':
			if($see_more > 4 ){
				$pH =  "6.8 - 7.2";
				$DO =  "5.00 - 6.00";
				$BOD = "1.8 - 1.5";
			}elseif($see_more >= 1 && $see_more <= 4){
				$pH =  "6.3 - 7.8";
				$DO =  "> 6.00";
				$BOD = "< 1.5";
			}else{
				$pH =  "นอกช่วง 6.0 - 8.0";
				$DO =  "นอกช่วง 4.00 - 5.00";
				$BOD = "> 2.0 - 1.8";
			}
			break;
			case '99':
			if($see_more >=4 ){
				$pH =  "6.8 - 7.2";
				$DO =  "> 6.00";
				$BOD = "< 1.5";
			}elseif($see_more >= 1 && $see_more <= 4){
				$pH =  "6.0 - 8.0";
				$DO =  "4.00 - 5.00";
				$BOD = "2.0 - 1.8";
			}else{
				$pH =  "นอกช่วง 5.5 - 8.5";
				$DO =  "นอกช่วง 3.00 - 4.00";
				$BOD = "3.0 - 2.0";
			}
			break;
			case '98':
			if($see_more >=4 ){
				$pH =  "6.8 - 7.2";
				$DO =  "> 6.00";
				$BOD = "< 1.5";
			}elseif($see_more >= 1 && $see_more <= 4){
				$pH =  "6.0 - 8.0";
				$DO =  "4.00 - 5.00";
				$BOD = "2.0 - 1.8";
			}else{
				$pH =  "นอกช่วง 5.5 - 8.5";
				$DO =  "นอกช่วง 3.00 - 4.00";
				$BOD = "3.0 - 2.0";
			}
			break;
			case '97':
			if($see_more >=4 ){
				$pH =  "6.3 - 7.8";
				$DO =  "5.00 - 6.00";
				$BOD = "1.8. - 1.5";
			}elseif($see_more >= 1 && $see_more <= 4){
				$pH =  "6.0 - 8.0";
				$DO =  "4.00 - 5.00";
				$BOD = "2.0 -1.8";
			}else{
				$pH =  "นอกช่วง 5.5 - 8.5";
				$DO =  "นอกช่วง 3.00 - 4.00";
				$BOD = "3.0 - 2.0";
			}
			break;
			case '96':
			if($see_more >=4 ){
				$pH =  "6.0 - 8.0";
				$DO =  "4.00 - 5.00";
				$BOD = "2.0 - 1.8";
			}elseif($see_more >= 1 && $see_more <= 4){
				$pH =  "6.0 - 7.2";
				$DO =  "> 6.00";
				$BOD = "< 1.5";
			}else{
				$pH =  "นอกช่วง 5.5 - 8.5";
				$DO =  "นอกช่วง 3.00 - 4.00";
				$BOD = "3.0 - 2.0";
			}
			break;
			case '95':
			if($see_more >=4 ){
				$pH =  "6.0 - 8.0";
				$DO =  "4.00 - 5.00";
				$BOD = "2.0 - 1.8";
			}elseif($see_more >= 1 && $see_more <= 4){
				$pH =  "6.8 - 7.2";
				$DO =  "> 6.00";
				$BOD = "< 1.5";
			}else{
				$pH =  "นอกช่วง 5.5 - 8.5";
				$DO =  "นอกช่วง 3.00 - 4.00";
				$BOD = "2.0";
			}
			break;
			case '94':
			if($see_more >=4 ){
				$pH =  "5.5 - 8.5";
				$DO =  "3.00 - 4.00";
				$BOD = "3.0 - 2.0";
			}elseif($see_more >= 1 && $see_more <= 4){
				$pH =  "6.0 - 8.0";
				$DO =  "4.00 - 5.00";
				$BOD = "2.0 - 1.8";
			}else{
				$pH =  "นอกช่วง 5.0 - 9.0";
				$DO =  "นอกช่วง 2.00 - 3.00";
				$BOD = "> 4.00";
			}
			break;
			case '93':
			if($see_more >=4 ){
				$pH =  "5.5 - 8.5";
				$DO =  "3.00 - 4.00";
				$BOD = "3.0 - 2.0";
			}elseif($see_more >= 1 && $see_more <= 4){
				$pH =  "6.0 - 8.0";
				$DO =  "4.00 - 5.00";
				$BOD = "2.0 - 1.8";
			}else{
				$pH =  "นอกช่วง 5.0 - 9.0";
				$DO =  "นอกช่วง 2.00 - 3.00";
				$BOD = "> 4.00";
			}
			break;
			default:
			$pH =  "นอกช่วง 5.0 - 9.0";
			$DO =  "นอกช่วง 2.00 - 3.00";
			$BOD = "> 4.00";
			break;
		}
		$this->data['pH'] = $pH;
		$this->data['DO'] = $DO;
		$this->data['BOD'] = $BOD;

		return $this->data;
	}

}

/* End of file Management.php */
/* Location: ./application/controllers/Management.php */