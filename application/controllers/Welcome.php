<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->layout = 'frontoffice/dashboard';
    if($this->session->userdata('userID')){
			$this->data['dataLogin'] = $this->session->userdata();
		}else{
			$this->data['dataLogin'] = null;
		}
	}

	public function index()
	{
		$this->app->render('Dashboard', 'index', $this->data , false);

	}

}
