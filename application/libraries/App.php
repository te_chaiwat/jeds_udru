<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App
{
  protected $ci;

  public function __construct()
  {
    $this->ci =& get_instance();
  }

  public function getLayout()
  {
    return $this->layout;
  }

  public function setLayout($var)
  {
    $this->layout = $var;
  }

  public function getTitle()
  {
    return $this->title;
  }

  public function setTitle($var)
  {
    $this->title = $var;
  }

  public function render($title = 'Check pH', $render, $data = null, $backoffice = true)
  {
    $this->setTitle($title);
    $view = $this->ci->load->view('public/' . $render, $data, true);
    $this->setLayout($view);
    if ($backoffice) {
      $this->ci->load->view('templates/frontoffice/content');
    } else {
      $this->ci->load->view('templates/frontoffice/content');
    }
  }

}

/* End of file App.php */
/* Location: ./application/libraries/App.php */
