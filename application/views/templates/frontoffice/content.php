<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>JEDs UDRU</title>
  <link rel="icon" href="<?php echo base_url('assets/jeds.ico'); ?>">
  <link rel="shortcut icon" href="<?php echo base_url('assets/jeds.ico'); ?>">
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.min.css') ?>">
  <script src="<?php echo base_url('assets/jquery/jquery.min.js');?>"></script>

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
      <a class="navbar-brand" href="<?php echo base_url(); ?>"><i class="fas fa-home"></i> JEDs UDRU</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url(); ?>"> หน้าหลัก
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('index.php/checkph'); ?>">เช็คค่า pH</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('index.php/checkColor'); ?>">เช็คค่า color</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="#">Services</a>
          </li> -->
          <?php if(!empty($this->data['dataLogin']['username'])): ?>
           <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $dataLogin['name']; ?></a>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="<?php echo base_url('index.php/authen/profile/'.$dataLogin['userID']); ?>"><i class="far fa-address-card"></i> แก้ไขข้อมูล</a>
              <?php if($dataLogin['status'] != 'public'): ?>
                <a class="dropdown-item" href="<?php echo base_url('index.php/Management/loglist'); ?>"><i class="fas fa-tools"></i> management </a>
              <?php endif; ?>
              <a class="dropdown-item" href="<?php echo base_url('index.php/authen/logout'); ?>"><i class="fa fa-power-off"></i> ออกจากระบบ</a>
            </div>
          </li>
          <?php else: ?>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url('index.php/authen') ?>">ลงชื่อเข้าใช้</a>
            </li>
          <?php endif; ?>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="row-fluid mt-3">

    <?php echo $this->app->getLayout(); ?>

  </div>


  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>


</body>

</html>
