<form action="<?php echo base_url('index.php/authen/updateProfile'); ?>" method="post">
	<input type="hidden" name="id" value="<?php echo $profile[0]['id']; ?>">
	<div class="col-sm-12">

		<!-- <div class="form-row">
			<div class="form-group col-md-6">
				<label for="username" class="col-form-label font-weight-bold"> ชื่อผู้ใช้:</label>
				<input type="text" name="username" id="username" class="form-control" value="<?php //echo $profile[0]['ac_username']; ?>" required readonly>
			</div>

			<div class="form-group col-md-6">
				<label for="password" class="col-form-label font-weight-bold"> รหัสผ่าน:</label>
				<input type="password" name="password" id="password" class="form-control" required >
			</div>
		</div> -->

		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="name" class="col-form-label font-weight-bold"> ชื่อ:</label>
				<input type="text" name="name" id="name" class="form-control" value="<?php echo $profile[0]['ac_name']; ?>" required >
			</div>

			<div class="form-group col-md-6">
				<label for="p_name1" class="col-form-label font-weight-bold"> สกุล:</label>
				<input type="text" name="lastname" id="name" class="form-control" value="<?php echo $profile[0]['ac_lastname']; ?>" required >
			</div>
		</div>

		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="school" class="col-form-label font-weight-bold"> โรงเรียน:</label>
				<input type="school" name="school" id="school" class="form-control" value="<?php echo $profile[0]['ac_school']; ?>" required >
			</div>
			<div class="form-group col-md-4">
				<label for="class" class="col-form-label font-weight-bold"> ชั้น:</label>
				<br>
				<?php $checked0 = ($profile[0]['ac_class'] == 'ป')? "checked":'' ; ?>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" id="class1" name="class" class="custom-control-input" value="ป" <?php echo $checked0; ?> >
					<label class="custom-control-label" for="class1"> ประถมศึกษา</label>
				</div>
				<?php $checked1 = ($profile[0]['ac_class'] == 'ม')? "checked":'' ; ?>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" id="class2" name="class" class="custom-control-input" value="ม" <?php echo $checked1; ?> >
					<label class="custom-control-label" for="class2"> มัธยมศึกษา</label>
				</div>
				<?php $checked2 = ($profile[0]['ac_class'] == 'อ')? "checked":'' ; ?>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" id="class3" name="class" class="custom-control-input" value="อ" <?php echo $checked2; ?> >
					<label class="custom-control-label" for="class3"> ครู/อาจารย์</label>
				</div>
			</div>
			<div class="col-md-2">
				<label for="classroom" class="col-form-label font-weight-bold"> ระดับ:</label>
				<input type="number" name="classroom" id="classroom" class="form-control" value="<?php echo $profile[0]['ac_classroom']; ?>" min='1' max='6'>
			</div>
			<!-- /.row col-5 -->

			<!-- /.form-group col-md-12 -->
		</div>
		<!-- /.form-row -->
	</div>
	<!-- ./ end col-sm-12 -->
	<div class="modal-footer alert-info" style="text-align:center;">
		<div class="mx-auto">
			<button type="submit" id="save" class="btn btn-success mr-2"><span class="   glyphicon glyphicon-floppy-saved"> บันทึก</span></button>
			<button type="reset" class="btn btn-danger " data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"> ยกเลิก</span></button>
		</div>
	</div>
</form>