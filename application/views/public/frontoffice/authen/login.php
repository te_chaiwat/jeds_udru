<div class="sufee-login d-flex align-content-center flex-wrap">
	<div class="container">
		<div class="login-content">
			<!-- <div class="login-logo">
				<a href="index.html">
					<img class="align-content" src="<?php echo base_url(); ?>assets/images/logo.png" alt="">
				</a>
			</div> -->
			<div class="form-row col-sm-8 mx-auto">
			 	<?php if (!empty($_SESSION['message'])) : ?>
			 		<div class="alert alert-danger alert-dismissible fade show col-sm-12" role="alert">
			 			<?php echo $_SESSION['message']; ?>
			 			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			 				<span aria-hidden="true">&times;</span>
			 			</button>
			 		</div>
			 	<?php endif; ?>
			 </div>
			<div class="login-form">
				<form action="<?php echo base_url('index.php/authen/checklogin'); ?>" method="post">
					<div class="form-group">
						<label>ชื่อผู้ใช้</label>
						<input type="text" class="form-control" name="username" autofocus>
					</div>
					<div class="form-group">
						<label>รหัสผ่าน</label>
						<input type="password" class="form-control" name="password">
					</div>
					<div class="col-12 col-sm-6 col-md-6 mx-auto clearfix">
						<button type="submit" class="btn btn-success btn-flat float-left col-5"> <i class="fa fa-key"></i> เข้าสู่ระบบ</button>
						<button type="reset" class="btn btn-warning btn-flat float-right col-5"> <i class="fa fa-times"></i> ยกเลิก</button>
					</div>
					<div class="register-link mt-1 text-center">
						<p> ยังไม่ได้เป็นสมาชิก ? <a href="<?php echo base_url('index.php/authen/regis'); ?>"> ลงทะเบียนที่นี่</a></p>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
