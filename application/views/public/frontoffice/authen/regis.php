<form action="<?php echo base_url('index.php/authen/addregis'); ?>" method="post">
	<div class="col-sm-12">

		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="username" class="col-form-label font-weight-bold"> ชื่อผู้ใช้:</label>
				<input type="text" name="username" id="username" class="form-control" required autofocus />
			</div>

			<div class="form-group col-md-6">
				<label for="password" class="col-form-label font-weight-bold"> รหัสผ่าน:</label>
				<input type="password" name="password" id="password" class="form-control" required />
			</div>
		</div>

		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="name" class="col-form-label font-weight-bold"> ชื่อ:</label>
				<input type="text" name="name" id="name" class="form-control" required >
			</div>

			<div class="form-group col-md-6">
				<label for="p_name1" class="col-form-label font-weight-bold"> สกุล:</label>
				<input type="text" name="lastname" id="name" class="form-control" required >
			</div>
		</div>

		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="school" class="col-form-label font-weight-bold"> โรงเรียน:</label>
				<input type="school" name="school" id="school" class="form-control" required >
			</div>
			<div class="form-group col-md-4">
				<label for="class" class="col-form-label font-weight-bold"> ชั้น/สถานะ:</label>
				<br>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" id="class1" name="class" class="custom-control-input" value="ป" checked >
					<label class="custom-control-label" for="class1"> ประถมศึกษา</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" id="class2" name="class" class="custom-control-input" value="ม" >
					<label class="custom-control-label" for="class2"> มัธยมศึกษา</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
					<input type="radio" id="class3" name="class" class="custom-control-input" value="อ" >
					<label class="custom-control-label" for="class3"> ครู/อาจารย์</label>
				</div>
			</div>
			<div class="col-md-2">
				<label for="classroom" class="col-form-label font-weight-bold"> ระดับ:</label>
				<input type="number" name="classroom" id="classroom" class="form-control" value="1" min='1' max='6'>
			</div>
			<!-- /.row col-5 -->

			<!-- /.form-group col-md-12 -->
		</div>
		<!-- /.form-row -->
	</div>
	<!-- ./ end col-sm-12 -->
	<div class="modal-footer alert-info" style="text-align:center;">
		<div class="mx-auto">
			<button type="submit" id="save" class="btn btn-success mr-2"><span class="   glyphicon glyphicon-floppy-saved"> บันทึก</span></button>
			<button type="reset" class="btn btn-danger " data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"> ยกเลิก</span></button>
		</div>
	</div>
</form>

<script type="text/javascript">
	$(function(){
		username_char();
		password_char();

	});

	function username_char() {
		$('#username').keyup(function(){
			if(!$(this).val().match(/^\w*[a-zA-Z]+\w*$/)){
				alert("กรอกได้เฉพาะตัวเลขและตัวอักษรภาษาอังกฤษเท่านั้น");
				$(this).val('');
			}
		});

	}

	function password_char() {
		$('#password').keyup(function(){
			if(!$(this).val().match(/^([a-z0-9])+$/i)){
				alert("กรอกได้เฉพาะตัวเลขและตัวอักษรภาษาอังกฤษเท่านั้น");
				$('#password').val('');
			}
		});

	}

</script>