  <link href="<?php echo base_url('assets/style/checkcolor.css') ?>" rel="stylesheet" type="text/css" />

  <div class="container">
      <div class="row">

        <div class="col-sm-6 col-md-6 col-xs-12">
            <canvas id="panel" width="500" height="333" ></canvas>
        </div>

        <div class="col-sm-6 col-md-6 col-xs-12">
            <div>
                <input type="file" name="image" id="file_upload" class="form-control">
            </div>
            <div>Preview:</div>
            <div id="preview"></div>
            <div>Color:</div>
            <div>R: <input type="text" id="rVal" /></div>
            <div>G: <input type="text" id="gVal" /></div>
            <div>B: <input type="text" id="bVal" /></div>
            <div>RGB: <input type="text" id="rgbVal" /></div>
            <div>RGBA: <input type="text" id="rgbaVal" /></div>
            <div>HEX: <input type="text" id="hexVal" /></div>
            <hr />
        </div>
        <div style="clear:both;"></div>
    </div>
</div>
<script>

    var canvas;
    var ctx;
    var URL = '<?php echo base_url();?>';

    var images = [ // predefined array of used images
    URL+'assets/images/noimage.png'];
    var iActiveImage = 0;

    $(function(){

    // drawing active image
    var image = new Image();
    image.onload = function () {
        ctx.drawImage(image, 0, 0, 500,333); // draw the image on the canvas
    }
    image.src = images[iActiveImage];

    // creating canvas object
    // canvas = document.getElementById('panel');
    canvas = $('#panel')[0];
    ctx = canvas.getContext('2d');

    $('#panel').mousemove(function(e) { // mouse move handler
        var canvasOffset = $(canvas).offset();
        var canvasX = Math.floor(e.pageX - canvasOffset.left);
        var canvasY = Math.floor(e.pageY - canvasOffset.top);

        var imageData = ctx.getImageData(canvasX, canvasY, 1, 1);
        var pixel = imageData.data;

        var pixelColor = "rgba("+pixel[0]+", "+pixel[1]+", "+pixel[2]+", "+pixel[3]+")";
        $('#preview').css('backgroundColor', pixelColor);
    });

    $('#panel').click(function(e) { // mouse click handler
        var canvasOffset = $(canvas).offset();
        var canvasX = Math.floor(e.pageX - canvasOffset.left);
        var canvasY = Math.floor(e.pageY - canvasOffset.top);

        var imageData = ctx.getImageData(canvasX, canvasY, 1, 1);
        var pixel = imageData.data;

        $('#rVal').val(pixel[0]);
        $('#gVal').val(pixel[1]);
        $('#bVal').val(pixel[2]);

        $('#rgbVal').val(pixel[0]+','+pixel[1]+','+pixel[2]);
        $('#rgbaVal').val(pixel[0]+','+pixel[1]+','+pixel[2]+','+pixel[3]);
        var dColor = pixel[2] + 256 * pixel[1] + 65536 * pixel[0];
        $('#hexVal').val( '#' + dColor.toString(16) );
    });

    $('#swImage').click(function(e) { // switching images
        iActiveImage++;
        if (iActiveImage >= 10) iActiveImage = 0;
        image.src = images[iActiveImage];
    });


    // canvas show picture
    $("#file_upload").change(function (e) {
      var F = this.files[0];
      var reader = new FileReader();
      reader.onload = imageIsLoaded;
      reader.readAsDataURL(F);
  });

    function imageIsLoaded(e) {
      var img = new Image();
      img.onload = function(){
        canvas.width  = "500";
        canvas.height = "333";
        // ctx.drawImage(this, 0, 0);
        // canvas.width  = this.width;
        // canvas.height = this.height;
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
    };
    img.src = e.target.result;
}

  //end show image //
});

</script>