<div class="container">
	<p>:: คุณภาพน้ำผิวดิน ::</p>
	<div class="input-group mb-3">
		<div class="input-group-prepend">
			<span class="input-group-text">pH : </span>
		</div>
		<input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" value="<?php echo $pH; ?>">
	</div>
	<div class="input-group mb-3">
		<div class="input-group-prepend">
			<span class="input-group-text">DO : </span>
		</div>
		<input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" value="<?php echo $DO; ?>">
		<div class="input-group-append">
			<span class="input-group-text">mg/L</span>
		</div>
	</div>
	<div class="input-group mb-3">
		<div class="input-group-prepend">
			<span class="input-group-text">BOD : </span>
		</div>
		<input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" value="<?php echo $BOD; ?>">
		<div class="input-group-append">
			<span class="input-group-text">mg/L</span>
		</div>
	</div>
</div>