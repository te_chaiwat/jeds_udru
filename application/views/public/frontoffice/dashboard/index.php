    <div class="col-sm-6 mx-auto ">
     <p> :: คุณภาพน้ำผิวดิน ::</p>

     <form action="<?php echo base_url('index.php/checkph/process'); ?>" method="post">
      <div class="form-row">

        <div class="form-group col-7 col-md-9 col-sm-9 mainAnimal" id="mainAnimal1">
          <label for="animal">สัตว์หน้าดิน</label>
          <select name="animal[]" id="animal1" class="form-control form-control-sm animal" tabindex="1" required>
            <option value="9" selected> --  กรุณากดตัวเลือก  -- </option>
            <?php $num=1; ?>
            <?php foreach ($animals as $animal) : ?>
              <option value="<?php echo $animal['id']; ?>"> <?php echo $num++.'. '.$animal['animal_name']; ?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="form-group col-3 col-md-2 col-sm-2">
          <label for="number">จำนวน</label>
          <input type="number" name="number[]" class="form-control form-control-sm" id="number" min="0" value="0">
        </div>
        <div class="col-2 col-sm-1 col-md-1">
          <label for="pluse">&nbsp;</label>
          <button type="button" class="btn btn-primary btn-sm col-12 col-sm-12 col-md-12" id="pluse">+</button>
        </div>
        <div class="form-group col-10 col-sm-10 col-md-10 mx-auto ">
          <label for="pluse"></label>
          <button type="submit" class="btn btn-success col-12 mx-auto"> Process</button>
        </div>

        <div id="addAnimal0" class="addAnimal col-sm-12 col-md-12">
          <!--=====================================
          =            show add animal            =
          ======================================-->
        </div>

      </div>
    </form>

    <div class="col-12">
    </div>
  </div>
  <!-- /.col-sm-12 -->

  <script>
   $(function(){
     addAnimal();
   });

   function addAnimal() {
    var num = $('.animal').length;

    $('#pluse').on('click',function(){
     var html ='<div class="animal_ form-row" id="animal_'+(num+1)+'">';
     html +=  '<div class="form-group col-7 col-md-9 col-sm-9">';
     html +='   <label for="animal'+(num+1)+'">สัตว์หน้าดิน</label>';
     html +='   <select name="animal[]" id="animal'+(num+1)+'" class="form-control form-control-sm animal" tabindex="1" required>';
     html +='     <option disabled selected> --  กรุณากดตัวเลือก  -- </option>';

     html +='   </select>';
     html +=' </div>';

     html += '<div class="form-group col-3 col-md-2 col-sm-2">';
     html +='   <label for="number'+(num+1)+'">จำนวน</label>';
     html +='   <input type="number" name="number[]" class="form-control form-control-sm" id="number'+(num+1)+'" min="0" value="0">';
     html +=' </div>';
     html +='  <div class="col-2 col-sm-1 col-md-1">';
     html +='   <label for="pluse">&nbsp;</label>';
     html +='   <button type="button" class="btn btn-danger btn-sm col-12 col-sm-12 col-md-12" id="delete'+(num+1)+'">-</button>';
     html +=' </div>';

     html +=' </div>';
     if( num <= 8){
       $('#addAnimal0').prepend(html);
       $('#number'+(num+1)).focus();
       deleteAnimal(num+1);
       getAnimal(num+1);
     }else{
      alert(" มากกว่าที่กำหนด ");
    }
    num++;

  });
  }

  function deleteAnimal(num) {
    $('#delete'+num).click(function(){
      var conf= confirm("ต้องการยกเลิกตัวเลือก");
      if(conf){
        $('#animal_'+num).remove();
      }
    });
  }

  function getAnimal(num) {
    $.ajax({
      url: '<?php echo base_url("index.php/checkph/getAnimalJson/");?>',
      dataType: 'json',
      success:function(res){
        var selected ="";
        $.each(res, function(index, value){
          selected +='<option value="'+value.id+'">'+value.animal_name+'</option>';
        });
        $('#animal'+num).append(selected).trigger('chosen:updated');
      },
      error:function(){
        console.log("ERROR");
      }
    });
  }
</script>