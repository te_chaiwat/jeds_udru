  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/DataTables/datatables.min.css');?>"/>

  <script type="text/javascript" src="<?php echo base_url('assets/DataTables/datatables.min.js');?>"></script>

  <div class="container">

    <p class="h2"> ประวัติการสำรวจค่า pH /ยังไม่เสร็จ</p>
    <hr>

    <table class="table table-striped border table-bordered table-sm" id="showAllUser">
      <thead class="thead-light text-center">
        <tr>
          <th scope="col">#</th>
          <th scope="col"> ชื่อ </th>
          <th scope="col"> สกุล </th>
          <th scope="col"> สัตว์ที่พบ </th>
          <th scope="col"> ค่าที่วัดได้ </th>
          <th scope="col"> โรงเรียน </th>
          <th scope="col"> วันที่ </th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($list as $key => $userRow): ?>
          <tr>
            <th scope="row"><?php //echo $num++; ?></th>
            <td> <?php echo $userRow['ac_name']; ?></td>
            <td> <?php echo $userRow['ac_lastname']; ?></td>
            <td>
              <?php $num = 1; ?>
              <?php

              foreach ($userRow['animals'] as $key => $value) {
                echo $num++.$value['animal_name'].',';
              }
              ?>
            </td>
            <td>
             <?php
             $a = array();
             $b = array();
             foreach ($userRow['animals'] as  $value1) {
             // echo  $b."<br>";
              array_push($a,$value1['animal_importance']) ;
              array_push($b,$value1['chck_see']);
            }

            switch ($a) {
              case '100':
              if($b > 4 ){
                echo  'pH ='.$pH =  "6.8 - 7.2 <br>";
                echo  'DO ='.$DO =  "5.00 - 6.00 <br>";
                echo  'BOD ='.$BOD = "1.8 - 1.5 <br>";
              }elseif($b >= 1 && $b <= 4){
                echo  'pH ='.$pH =  "6.3 - 7.8 <br>";
                echo  'DO ='.$DO =  "> 6.00 <br>";
                echo  'BOD ='.$BOD = "< 1.5 <br>";
              }else{
                echo  'pH ='.$pH =  "นอกช่วง 6.0 - 8.0 <br>";
                echo  'DO ='.$DO =  "นอกช่วง 4.00 - 5.00 <br>";
                echo  'BOD ='.$BOD = "> 2.0 - 1.8 <br>";
              }
              break;
              case '99':
              if($b >=4 ){
                echo  'pH ='.$pH =  "6.8 - 7.2 <br>";
                echo  'DO ='.$DO =  "> 6.00 <br>";
                echo  'BOD ='.$BOD = "< 1.5 <br>";
              }elseif($b >= 1 && $b <= 4){
                echo  'pH ='.$pH =  "6.0 - 8.0 <br>";
                echo  'DO ='.$DO =  "4.00 - 5.00 <br>";
                echo  'BOD ='.$BOD = "2.0 - 1.8 <br>";
              }else{
                echo  'pH ='.$pH =  "นอกช่วง 5.5 - 8.5 <br>";
                echo  'DO ='.$DO =  "นอกช่วง 3.00 - 4.00 <br>";
                echo  'BOD ='.$BOD = "3.0 - 2.0 <br>";
              }
              break;
              case '98':
              if($b >=4 ){
                echo  'pH ='.$pH =  "6.8 - 7.2 <br>";
                echo  'DO ='.$DO =  "> 6.00 <br>";
                echo  'BOD ='.$BOD = "< 1.5 <br>";
              }elseif($b >= 1 && $b <= 4){
                echo  'pH ='.$pH =  "6.0 - 8.0 <br>";
                echo  'DO ='.$DO =  "4.00 - 5.00 <br>";
                echo  'BOD ='.$BOD = "2.0 - 1.8 <br>";
              }else{
                echo  'pH ='.$pH =  "นอกช่วง 5.5 - 8.5 <br>";
                echo  'DO ='.$DO =  "นอกช่วง 3.00 - 4.00 <br>";
                echo  'BOD ='.$BOD = "3.0 - 2.0 <br>";
              }
              break;
              case '97':
              if($b >=4 ){
                echo  'pH ='.$pH =  "6.3 - 7.8 <br>";
                echo  'DO ='.$DO =  "5.00 - 6.00 <br>";
                echo  'BOD ='.$BOD = "1.8. - 1.5 <br>";
              }elseif($b >= 1 && $b <= 4){
                echo  'pH ='.$pH =  "6.0 - 8.0 <br>";
                echo  'DO ='.$DO =  "4.00 - 5.00 <br>";
                echo  'BOD ='.$BOD = "2.0 -1.8 <br>";
              }else{
                echo  'pH ='.$pH =  "นอกช่วง 5.5 - 8.5 <br>";
                echo  'DO ='.$DO =  "นอกช่วง 3.00 - 4.00 <br>";
                echo  'BOD ='.$BOD = "3.0 - 2.0 <br>";
              }
              break;
              case '96':
              if($b >=4 ){
                echo  'pH ='.$pH =  "6.0 - 8.0 <br>";
                echo  'DO ='.$DO =  "4.00 - 5.00 <br>";
                echo  'BOD ='.$BOD = "2.0 - 1.8 <br>";
              }elseif($b >= 1 && $b <= 4){
                echo  'pH ='.$pH =  "6.0 - 7.2 <br>";
                echo  'DO ='.$DO =  "> 6.00 <br>";
                echo  'BOD ='.$BOD = "< 1.5 <br>";
              }else{
                echo  'pH ='.$pH =  "นอกช่วง 5.5 - 8.5 <br>";
                echo  'DO ='.$DO =  "นอกช่วง 3.00 - 4.00 <br>";
                echo  'BOD ='.$BOD = "3.0 - 2.0 <br>";
              }
              break;
              case '95':
              if($b >=4 ){
                echo  'pH ='.$pH =  "6.0 - 8.0 <br>";
                echo  'DO ='.$DO =  "4.00 - 5.00 <br>";
                echo  'BOD ='.$BOD = "2.0 - 1.8 <br>";
              }elseif($b >= 1 && $b <= 4){
                echo  'pH ='.$pH =  "6.8 - 7.2 <br>";
                echo  'DO ='.$DO =  "> 6.00 <br>";
                echo  'BOD ='.$BOD = "< 1.5 <br>";
              }else{
                echo  'pH ='.$pH =  "นอกช่วง 5.5 - 8.5 <br>";
                echo  'DO ='.$DO =  "นอกช่วง 3.00 - 4.00 <br>";
                echo  'BOD ='.$BOD = "2.0 <br>";
              }
              break;
              case '94':
              if($b >=4 ){
                echo  'pH ='.$pH =  "5.5 - 8.5 <br>";
                echo  'DO ='.$DO =  "3.00 - 4.00 <br>";
                echo  'BOD ='.$BOD = "3.0 - 2.0 <br>";
              }elseif($b >= 1 && $b <= 4){
                echo  'pH ='.$pH =  "6.0 - 8.0 <br>";
                echo  'DO ='.$DO =  "4.00 - 5.00 <br>";
                echo  'BOD ='.$BOD = "2.0 - 1.8 <br>";
              }else{
                echo  'pH ='.$pH =  "นอกช่วง 5.0 - 9.0 <br>";
                echo  'DO ='.$DO =  "นอกช่วง 2.00 - 3.00 <br>";
                echo  'BOD ='.$BOD = "> 4.00 <br>";
              }
              break;
              case '93':
              if($b >=4 ){
                echo  'pH ='.$pH =  "5.5 - 8.5 <br>";
                echo  'DO ='.$DO =  "3.00 - 4.00 <br>";
                echo  'BOD ='.$BOD = "3.0 - 2.0 <br>";
              }elseif($b >= 1 && $b <= 4){
                echo  'pH ='.$pH =  "6.0 - 8.0 <br>";
                echo  'DO ='.$DO =  "4.00 - 5.00 <br>";
                echo  'BOD ='.$BOD = "2.0 - 1.8 <br>";
              }else{
                echo  'pH ='.$pH =  "นอกช่วง 5.0 - 9.0 <br>";
                echo  'DO ='.$DO =  "นอกช่วง 2.00 - 3.00 <br>";
                echo  'BOD ='.$BOD = "> 4.00 <br>";
              }
              break;
              default:
              echo  'pH ='.$pH =  "นอกช่วง 5.0 - 9.0 <br>";
              echo  'DO ='.$DO =  "นอกช่วง 2.00 - 3.00 <br>";
              echo  'BOD ='.$BOD = "> 4.00 <br>";
              break;
            }


            ?>
          </td>
          <td> <?php echo $userRow['ac_school']; ?></td>
          <td> <?php echo $userRow['create_at']; ?></td>
        </tr>
      <?php endforeach ?>
    </tbody>
  </table>
</div>
<!-- /.container -->


<script type="text/javascript" charset="utf-8">
  $(document).ready(function () {
    $('#showAllUser').DataTable({
     pagingType: 'full',
     "columns": [{
      "width": "5%",
      "sClass": "text-center"
    }, {
      "width": "15%"
    }, {
      "width": "15%"
    }, {
      "width": "15%"
    }, {
      "width": "20%",
      "sClass": "center"
    }, {
      "width": "20%",
      "sClass": "center"
    }, {
      "width": "20%",
      "sClass": "text-center"
    }],
    responsive: true,
    language: {
      "zeroRecords": "============== ไม่พบข้อมูลที่ค้นหา ==============",
      "sEmptyTable": "ไม่มีข้อมูลในตาราง",
      "sSearch": "ค้นหา: ",
      "sLengthMenu": "แสดง _MENU_ รายการ",
    },
    "order": [[ 4, 'asc' ], ],

  });

  });
</script>