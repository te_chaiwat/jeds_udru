<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/DataTables/datatables.min.css');?>"/>

<script type="text/javascript" src="<?php echo base_url('assets/DataTables/datatables.min.js');?>"></script>

<div class="container">

  <p class="h2">สมาชิกทั้งหมด</p>
  <hr>

  <table class="table table-striped border table-bordered" id="showAllUser">
    <thead class="thead-light text-center">
      <tr>
        <th scope="col">#</th>
        <th scope="col"> ชื่อ </th>
        <th scope="col"> สกุล </th>
        <th scope="col"> ค่าที่วัดได้ </th>
        <th scope="col"> โรงเรียน </th>
        <th scope="col"> สถานะ </th>
      </tr>
    </thead>
    <tbody>
      <?php $num = 1; ?>
      <?php foreach ($userAll as $userRow): ?>
        <tr>
          <th scope="row"><?php echo $num++; ?></th>
          <td> <?php echo $userRow['ac_name']; ?></td>
          <td> <?php echo $userRow['ac_lastname']; ?></td>
          <td> <?php echo $userRow['ac_school']; ?></td>
          <td> <?php echo $userRow['create_at']; ?></td>
          <td> <?php echo $userRow['ac_status']; ?></td>
        </tr>
      <?php endforeach ?>
    </tbody>
  </table>
</div>
<!-- /.container -->


<script type="text/javascript" charset="utf-8">
  $(document).ready(function () {
    $('#showAllUser').DataTable({
     pagingType: 'full',
     "columns": [{
      "width": "5%",
      "sClass": "text-center"
    }, {
      "width": "15%"
    }, {
      "width": "15%"
    }, {
      "width": "10%",
      "sClass": "center"
    }, {
      "width": "20%",
      "sClass": "center"
    }, {
      "width": "10%",
      "sClass": "text-center"
    }],
    responsive: true,
    language: {
      "zeroRecords": "============== ไม่พบข้อมูลที่ค้นหา ==============",
      "sEmptyTable": "ไม่มีข้อมูลในตาราง",
      "sSearch": "ค้นหา: ",
      "sLengthMenu": "แสดง _MENU_ รายการ",
    },
     "order": [[ 0, 'asc' ], ],

  });

  });
</script>