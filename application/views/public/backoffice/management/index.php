<div class="container">

	<div class="row">

		<div class=" col-sm-3 mx-auto">
			<div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
				<div class="card-header">จัดการ ผู้ใช้</div>
				<div class="card-body text-center">
					<a href="<?php echo base_url('index.php/management/manageUser'); ?>" class="text-white">
						<i class="far fa-address-card fa-7x"></i>
						<p class="card-text"> จัดการผู้ใช้งาน</p>
					</a>
				</div>
			</div>
		</div>

		<div class=" col-sm-3 mx-auto">
			<div class="card text-white bg-info mb-3" style="max-width: 18rem;">
				<div class="card-header">ประวัติการวัดคุณภาพน้ำ</div>
				<div class="card-body text-center">
					<a href="<?php echo base_url('index.php/management/history'); ?>" class="text-white">
						<i class="fas fa-tasks fa-7x"></i>
						<p class="card-text">ประวัติการวัดคุณภาพ </p>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.container -->
